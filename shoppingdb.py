from kivy.app import App
from kivy.uix.boxlayout import BoxLayout
from kivy.properties import ObjectProperty
from kivy.uix.listview import ListItemButton


class ShoppingListButton(ListItemButton):
    pass


class ShoppingDB(BoxLayout):

   
    product_name_text_input = ObjectProperty()
    price_text_input = ObjectProperty()
    shopping_list = ObjectProperty()

    def submit_shopping(self):

        product_price = self.product_text_input.text + " " + self.price_text_input.text

        self.shopping_list.adapter.data.extend([product_price])

        self.shopping_list._trigger_reset_populate()

    def delete_shopping(self, *args):

        if self.shopping_list.adapter.selection:

            selection = self.shopping_list.adapter.selection[0].text

            self.shopping_list.adapter.data.remove(selection)

            self.shopping_list._trigger_reset_populate()

    def replace_shopping(self, *args):

        if self.shopping_list.adapter.selection:

            selection = self.shopping_list.adapter.selection[0].text

            self.shopping_list.adapter.data.remove(selection)

            product_price = self.product_text_input.text + " " + self.price_text_input.text

            self.shopping_list.adapter.data.extend([product_price])

            self.shopping_list._trigger_reset_populate()


class ShoppingDBApp(App):
    def build(self):
        return ShoppingDB()


dbApp = ShoppingDBApp()

dbApp.run()
